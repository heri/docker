Image containing Jenkins and several build tools:

* Java 8
* Maven 3.2.5
* Gradle 2.3
* Android SDK r24.1.2
* Android build tools 22.0.1
* Android API 22