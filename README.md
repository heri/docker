#Jenkins

**Build**

    docker build -t "frostcode/jenkins:latest" jenkins
    
**Run**

    docker run -p 8080:8080 -v /your/home:/var/jenkins frostcode/jenkins
